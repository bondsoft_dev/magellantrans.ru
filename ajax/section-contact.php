<div class="section-contact" id="section-contact">
    <div class="head">Контакты<br/>
        компании</div>
    <div class="content">
        <div class="address-wrap">
            <div class="title">Адрес</div>
            <div class="address">
                <div class="tit">Центральный офис</div>
                <p>ООО «Магеллан», 350901, г. Краснодар, ул.Первомайская, 69, оф. 207</p>
                <p>e-mail: info@magellantrans.ru, тел/факс: +7(861) 252-82-08, 252-87-38, тел/факс: +7(861) 25-777-95, 25-757-00</p>
            </div>
        </div>
        <div class="title">Отдел продаж</div>
        <div class="team-wrap">
            <div class="item">
                <div class="image">
                    <img src="/h/images/photo_1.png" alt=""/>
                </div>
                <div class="text">
                    <div class="name">Седов Валентин<br/>
                        Валерьевич</div>
                    <div class="position">Директор по продажам</div>
                    <ul>
                        <li>Тел: +7 (495) 961-2555 (многоканальный)</li>
                        <li>E-mail: info@albatroscargo.ru</li>
                        <li>Адрес: г. Москва,</li>
                        <li>ул. Садовническая, д. 76</li>
                    </ul>
                </div>
            </div>
            <div class="item">
                <div class="image">
                    <img src="/h/images/photo_2.png" alt=""/>
                </div>
                <div class="text">
                    <div class="name">Седов Валентин<br/>
                        Валерьевич</div>
                    <div class="position">Директор по продажам</div>
                    <ul>
                        <li>Тел: +7 (495) 961-2555 (многоканальный)</li>
                        <li>E-mail: info@albatroscargo.ru</li>
                        <li>Адрес: г. Москва,</li>
                        <li>ул. Садовническая, д. 76</li>
                    </ul>
                </div>
            </div>
            <div class="item">
                <div class="image">
                    <img src="/h/images/photo_3.png" alt=""/>
                </div>
                <div class="text">
                    <div class="name">Седов Валентин<br/>
                        Валерьевич</div>
                    <div class="position">Директор по продажам</div>
                    <ul>
                        <li>Тел: +7 (495) 961-2555 (многоканальный)</li>
                        <li>E-mail: info@albatroscargo.ru</li>
                        <li>Адрес: г. Москва,</li>
                        <li>ул. Садовническая, д. 76</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="title">Отдел логистики</div>
        <div class="team-wrap">
            <div class="item">
                <div class="image">
                    <img src="/h/images/photo_4.png" alt=""/>
                </div>
                <div class="text">
                    <div class="name">Седов Валентин<br/>
                        Валерьевич</div>
                    <div class="position">Директор по продажам</div>
                    <ul>
                        <li>Тел: +7 (495) 961-2555 (многоканальный)</li>
                        <li>E-mail: info@albatroscargo.ru</li>
                        <li>Адрес: г. Москва,</li>
                        <li>ул. Садовническая, д. 76</li>
                    </ul>
                </div>
            </div>
            <div class="item">
                <div class="image">
                    <img src="/h/images/photo_4.png" alt=""/>
                </div>
                <div class="text">
                    <div class="name">Седов Валентин<br/>
                        Валерьевич</div>
                    <div class="position">Директор по продажам</div>
                    <ul>
                        <li>Тел: +7 (495) 961-2555 (многоканальный)</li>
                        <li>E-mail: info@albatroscargo.ru</li>
                        <li>Адрес: г. Москва,</li>
                        <li>ул. Садовническая, д. 76</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="map" id="map">

    </div>
</div>