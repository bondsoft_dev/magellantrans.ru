<div class="section-media" id="section-media">
    <div class="head">
        медиа<br/>
        галерея
    </div>
    <div class="content">
        <div class="slider slider-for">
            <div>
                <div class="image" style="background-image: url('/h/images/gallery-min1.png')"></div>
            </div>
            <div>
                <div class="image" style="background-image: url('/h/images/gallery-image.png')"></div>
            </div>
            <div>
                <div class="image" style="background-image: url('/h/images/gallery-image.png')"></div>
            </div>
            <div>
                <div class="image" style="background-image: url('/h/images/gallery-image.png')"></div>
            </div>
        </div>
        <div class="panel">
            <div class="title">подгалереи</div>
            <div class="slider slider-nav">
                <div>
                    <div class="item">
                        <div class="image">
                            <img src="/h/images/gallery-min1.png" alt=""/>
                        </div>
                        <div class="tit">12/08 Название подгалереи типа рабочие будни</div>
                    </div>
                </div>
                <div>
                    <div class="item">
                        <div class="image">
                            <img src="/h/images/gallery-min2.png" alt=""/>
                        </div>
                        <div class="tit">12/08 Название подгалереи типа рабочие будни</div>
                    </div>
                </div>
                <div>
                    <div class="item">
                        <div class="image">
                            <img src="/h/images/gallery-min1.png" alt=""/>
                        </div>
                        <div class="tit">12/08 Название подгалереи типа рабочие будни</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>