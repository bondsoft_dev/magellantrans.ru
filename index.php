<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("tags", "Перевозка крупногабаритных грузов, сопровождение, согласование с ГИБДД, негабарит, по России");
$APPLICATION->SetPageProperty("keywords_inner", "Перевозка крупногабаритных грузов, сопровождение, согласование с ГИБДД");
$APPLICATION->SetPageProperty("title", "Маггелан - Перевозка крупногабаритных грузов по России");
$APPLICATION->SetPageProperty("keywords", "Перевозка крупногабаритных грузов, сопровождение, согласование с ГИБДД");
$APPLICATION->SetPageProperty("description", "Перевозка крупногабаритных грузов");
$APPLICATION->SetTitle("Magellan");
?><div id="page">
	<div class="section-wrap">
		 <div id="background">
            <div id="car"></div>
            <div id="trail"></div>
        </div>
		 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/about.php"
	)
);?> <script>
            $('#make-order-btn').click(function (event) {
                $('header nav a').removeClass('active');
                $('#section2').addClass('active');
                $('#page').removeClass('section1').removeClass('section3').removeClass('section6').removeClass('section4').removeClass('section5').addClass('section2');
                $.ajax({
                    url: "/order/?ajax=Y",
                    type: "GET",
                    cache:true,
                    contentType: "text/html;charset=utf-8",
                    success: function(data){
                        $('#section-bid').remove();
                        $(".section-wrap").append(data);
                    }
                });
                event.preventDefault();
            })
        </script>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>