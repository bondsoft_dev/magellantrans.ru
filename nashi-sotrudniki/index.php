<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Наши сотрудники");?>




<div class="sotrudniki">
	<div class="sotrudniki-title">
            <p>Опыт наших сотрудников в области логистики и планирования перевозок, позволяет нам принимать решения, полностью соответствующие&nbsp;задачам вашего бизнеса.</p>
	</div>
	<div class="sotrudniki-list">
		<ul>
			<li>
				<div class="sotrudniki_info">
                    <h5>ВИКТОР АЛЕКСАНДРОВИЧ</h5>
                        <div class="staff_department">Представительство в Ростове-на-Дону</div>
						<div class="staff_email">
							<a href="mailto:125@magellantrans.ru">125@magellantrans.ru</a>
						</div>
						<div class="staff_skype"> <p>125@magellantrans.ru</p> </div>
				</div>
            </li>
            <li>
				<div class="sotrudniki_info">
                    <h5>ГЛЕБ ВАЛЕРЬЕВИЧ</h5>
                        <div class="staff_department">Представительство в Санкт-Петербурге</div>
						<div class="staff_email">
							<a href="mailto:g.vorobyov@magellantrans.ru">g.vorobyov@magellantrans.ru</a>
						</div>
						<div class="staff_skype"> <p>g.vorobyov@magellantrans.ru</p> </div>
						<ul class="staff_socials">
						</ul>
				</div>
            </li>
            <li>
				<div class="sotrudniki_info">
                    <h5>ДАРЬЯ ЮРЬЕВНА</h5>
                    <div class="staff_department">Представительство в Москве</div>
					<div class="staff_email">
						<a href="mailto:d.liz@magellantrans.ru">d.liz@magellantrans.ru</a>
					</div>
					<div class="staff_skype"> <p>Alfa-daxa</p> </div>
					<ul class="staff_socials">
					</ul>
				</div>
            </li>
            <li>
				<div class="sotrudniki_info">
                    <h5>ЭДУАРД ВЛАДИМИРОВИЧ</h5>
                        <div class="staff_department">Отдел перевозок</div>
						<div class="staff_email">
							<a href="mailto:35@magellantrans.ru">35@magellantrans.ru</a>
						</div>
						<div class="staff_skype"> <p>125@magellantrans.ru</p> </div>
						<ul class="staff_socials">
						</ul>
				</div>
            </li>
            <li>
				<div class="sotrudniki_info">
                    <h5>ЮЛИЯ ЮРЬЕВНА</h5>
                        <div class="staff_department">Отдел международных перевозок</div>
						<div class="staff_email">
							<a href="mailto:34@magellantrans.ru">34@magellantrans.ru</a>
						</div>
						<div class="staff_skype">
                           <p>Julia-major2010</p> 
                        </div>
						<ul class="staff_socials">
						</ul>
				</div>
            </li>
            <li>
				<div class="sotrudniki_info">
                    <h5>ЕВГЕНИЙ ВЛАДИМИРОВИЧ</h5>
                        <div class="staff_department">Руководитель отдела негабаритных перевозок</div>
						<div class="staff_email">
							<a href="mailto:01@magellantrans.ru">01@magellantrans.ru</a>
						</div>
						<div class="staff_skype"> <p>evgeniy183183</p> </div>
						<ul class="staff_socials">
						</ul>
				</div>
			</li>
        </ul>
        <div>
            <h2>Наши сотрудники - <br>ваш успех</h2>
        </div>
	</div>
</div>





<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>