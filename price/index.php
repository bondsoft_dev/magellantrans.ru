<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Стоимость услуг перевозки грузов по России: цены на транспортные услуги");
?>

<div id="container-price" class="container">

					<div class="wpb_text_column">
				<h2 id="title-price" >Тарифы на грузоперевозки ТК «Магеллан»</h2>
<table class="tablepice" style="width: 700px;" border="2" cellspacing="1" cellpadding="1">
<tbody>
<tr>
<td>Услуга</td>
<td>Стоимость (за 1 км)</td>
<td>Валюта</td>
</tr>
<tr>
<td>Международные перевозки</td>
<td style="text-align: center;">от 1</td>
<td style="text-align: center;">€</td>
</tr>
<tr>
<td>Перевозка габаритных грузов</td>
<td style="text-align: center;">от 70</td>
<td style="text-align: center;">руб</td>
</tr>
<tr>
<td>Перевозка негабаритных грузов</td>
<td style="text-align: center;">от 90</td>
<td style="text-align: center;">руб</td>
</tr>
<tr>
<td>Перевозка металлоконструкций</td>
<td style="text-align: center;">от 70</td>
<td style="text-align: center;">руб</td>
</tr>
<tr>
<td>Перевозка промышленного оборудования</td>
<td style="text-align: center;">от 70</td>
<td style="text-align: center;">руб</td>
</tr>
<tr>
<td>Перевозка тракторов</td>
<td style="text-align: center;">от 70</td>
<td style="text-align: center;">руб</td>
</tr>
<tr>
<td>Перевозка комбайнов</td>
<td style="text-align: center;">от 70</td>
<td style="text-align: center;">руб</td>
</tr>
<tr>
<td>Перевозка нефтяного оборудования</td>
<td style="text-align: center;">от 70</td>
<td style="text-align: center;">руб</td>
</tr>
<tr>
<td>Перевозка газового оборудования</td>
<td style="text-align: center;">от 70</td>
<td style="text-align: center;">руб</td>
</tr>
<tr>
<td>Перевозка блок-боксов</td>
<td style="text-align: center;">от 70</td>
<td style="text-align: center;">руб</td>
</tr>
<tr>
<td>Перевозка компрессорных станций</td>
<td style="text-align: center;">от 70</td>
<td style="text-align: center;">руб</td>
</tr>
<tr>
<td>Перевозка строительной техники</td>
<td style="text-align: center;">от 70</td>
<td style="text-align: center;">руб</td>
</tr>
<tr>
<td>Перевозка строительных кранов</td>
<td style="text-align: center;">от 70</td>
<td style="text-align: center;">руб</td>
</tr>
<tr>
<td>Перевозка экскаваторов</td>
<td style="text-align: center;">от 70</td>
<td style="text-align: center;">руб</td>
</tr>
<tr>
<td>Перевозка бульдозеров</td>
<td style="text-align: center;">от 70</td>
<td style="text-align: center;">руб</td>
</tr>
<tr>
<td>Перевозка автогрейдеров</td>
<td style="text-align: center;">от 70</td>
<td style="text-align: center;">руб</td>
</tr>
<tr>
<td>Перевозка дорожных катков</td>
<td style="text-align: center;">от 70</td>
<td style="text-align: center;">руб</td>
</tr>
<tr>
<td>Перевозка мостовых пролетов</td>
<td style="text-align: center;">от 70</td>
<td style="text-align: center;">руб</td>
</tr>
</tbody>
</table>




			</div>
							</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>