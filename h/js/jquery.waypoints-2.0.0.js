(function($) {
  var temp = false;
  var temp2 = [];
  var tAngle = [];
  
  var works;
  var processID;
  //var e = {};
  var systemID = 0, data = {}, options = {}, methods = {
  init:function(settings) {
    return this.each(function() {
      options = jQuery.extend({
	    angle:0
		,direction:true
		,arrivalThreshold:50
		,interval:15
		,item:false
		,items:false
		,mass:1
		,maxForce:0.05
		,obj:this
		,pathThreshold:50
		,position:[0, 0]
		,rotate:true
		,speedLimit:2
		,waypoint:0
		,waypointEnd:false
		,waypoints:false
		,pricep:false
	  }, settings);
      if(!options.items.length && options.item && $(options.item).length) {
        options.items = [[options.item, options.waypoints]]
      }
      if(options.items.length) {
        $.each(options.items, function(i, el) {
          var e = {};
          e.obj = $(options.obj).find(el[0]);
          e.pricep = el[2] || options.pricep;
          e.waypointEnd = options.waypointEnd;
          e.direction = options.direction;
          e.waypoints = el[1] || options.waypoints;
          e.itemWidth = e.obj.width();
          e.itemHeight = e.obj.height();
          e.itemWidth05 = e.itemWidth / 2;
          e.itemHeight05 = e.itemHeight / 2;
          e.angle = el[11] || options.angle;
          e.arrivalThreshold = el[9] || options.arrivalThreshold;
          e.mass = el[7] || options.mass;
          e.maxForce = el[5] || options.maxForce;
          e.speedLimit = el[4] || options.speedLimit;
          e.pathThreshold = el[8] || options.pathThreshold;
          e.position = el[10] || options.position;
          e.waypoint = el[6] || options.waypoint;
          e.steeringForce = [0, 0];
          e.velocity = [0, 0];
          options.items[i] = e
        });
        processID = methods.getID();
		data[processID] = options;
				works = setInterval(function() {
					methods.process(processID);
				}, options.interval)
      }
    })
  }, process:function(processID) {
    return $.each(data[processID].items, function(i, e) {
      e = methods.follow(e);
      if(e) {
        e = methods.update(e);
        methods.render(e);
        data[processID].items[i] = e
      }else{
		  clearInterval(works);
	  }
    })
  }, 
//  
  follow:function(e) {
    var wayPoint = e.waypoints[e.waypoint];
	// делаем разворот, 
	// сырая функция
	/*
	if(temp){
		e.waypoint++
		if(e.waypoint >= e.waypointEnd ){
			e.waypoint = temp2[0];
			e.waypointEnd = temp2[1];
			e.waypoints = temp2[2];
		temp = false;
		}
	}
	//*/
//console.log('направление', e.direction,'текущая точка', e.waypoint,'конечная точка', e.waypointEnd);
    if(wayPoint == null) {
      return false
    }
	if(methods.dist(e.position, wayPoint) < e.pathThreshold) {
		  if(e.direction){
			  if( (e.waypoint >= e.waypoints.length - 1 || e.waypoint >= e.waypointEnd)) {
				clearInterval(works);
			  }else{
				   e.waypoint++
			  }  
		  }else{
			  if( (e.waypoint <= 0 || e.waypoint <= e.waypointEnd)) {
				clearInterval(works);
			  }else{
					if(e.waypoint >0 || e.waypoint > e.waypointEnd)
					e.waypoint-- 
			  }
		}
	}
	
	if(e.direction){
		if( (e.waypoint >= e.waypoints.length - 1 || e.waypoint >= e.waypointEnd )) {
			//тормозит 
		  e = methods.arrive(e, wayPoint)
		}else {
			//едет
		  e = methods.seek(e, wayPoint)
		}	
	}else{
		if( (e.waypoint <= 0 || e.waypoint <= e.waypointEnd )) {
			//тормозит 
		  e = methods.arrive(e, wayPoint)
		}else {
			//едет
		  e = methods.seek(e, wayPoint)
		}	
	}
	
    
    return e
	//конец	
  }, getRandom:function(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min
  }, getRandomFloat:function(min, max) {
    return Math.random() * (max - min) + min
  }, setAngle:function(obj, value) {
    var len = methods.getLength(obj);
    obj[0] = Math.cos(value) * len;
    obj[1] = Math.sin(value) * len;
    return obj
  }, getAngle:function(obj) {
    return Math.atan2(obj[1], obj[0])
  }, dist:function(obj, obj2) {
    return Math.sqrt(methods.distSQ(obj, obj2))
  }, distSQ:function(obj, obj2) {
    var dx = obj2[0] - obj[0];
    var dy = obj2[1] - obj[1];
    return dx * dx + dy * dy
  }, subtract:function(v1, v2) {
    return[v1[0] - v2[0], v1[1] - v2[1]]
  }, normalize:function(obj) {
    var len = methods.getLength(obj);
    if(len == 0) {
      obj[0] = 1;
      obj[1] = 1;
      return obj
    }
    obj[0] /= len;
    obj[1] /= len;
    return obj
  }, setLength:function(obj, value) {
    var a = methods.getAngle(obj);
    obj[0] = Math.cos(a) * value;
    obj[1] = Math.sin(a) * value;
    return obj
  }, getLength:function(obj) {
    return Math.sqrt(methods.getLengthSQ(obj))
  }, getLengthSQ:function(obj) {
    return obj[0] * obj[0] + obj[1] * obj[1]
  }, multiply:function(obj, value) {
    return[obj[0] * value, obj[1] * value]
  }, add:function(obj, v2) {
    return[obj[0] + v2[0], obj[1] + v2[1]]
  }, divide:function(obj, value) {
    return[obj[0] / value, obj[1] / value]
  }, truncate:function(obj, max) {
    var _length = Math.min(max, methods.getLength(obj));
    obj = methods.setLength(obj, _length);
    return obj
  }, seek:function(e, target) {
    var desiredVelocity = methods.subtract(target, e.position);
    desiredVelocity = methods.normalize(desiredVelocity);
    desiredVelocity = methods.multiply(desiredVelocity, e.speedLimit);
    var force = methods.subtract(desiredVelocity, e.velocity);
    e.steeringForce = methods.add(e.steeringForce, force);
    return e
  },
  arrive:function(e, target) {
	console.log('начинаем тормозить');
    var desiredVelocity = methods.subtract(target, e.position);
    desiredVelocity = methods.normalize(desiredVelocity);
    var _dist = methods.dist(e.position, target);
    //if(_dist < 1) {
	  //console.log('остановился');
      //e.steeringForce = 0;
      //return false
    //}
    if(_dist > e.arrivalThreshold) {
      desiredVelocity = methods.multiply(desiredVelocity, e.speedLimit)
    }else {
      desiredVelocity = methods.multiply(desiredVelocity, e.speedLimit * _dist / e.arrivalThreshold)
    }
    var force = methods.subtract(desiredVelocity, e.velocity);
    e.steeringForce = methods.add(e.steeringForce, force);
    return e
  }, update:function(e) {
		e.steeringForce = methods.truncate(e.steeringForce, e.maxForce);
		e.steeringForce = methods.divide(e.steeringForce, e.mass);
		e.velocity = methods.add(e.velocity, e.steeringForce);
		e.steeringForce = [0, 0];
		e.velocity = methods.truncate(e.velocity, e.speedLimit);
		e.position = methods.add(e.position, e.velocity);
		e.angle = methods.getAngle(e.velocity) * 180 / Math.PI;
		e.angle += 90;
    return e
  }, render:function(e) {
	  
		$("body").scrollTop(e.position[1] - screen.height/4)
		$("body").scrollLeft(e.position[0] - screen.width/4)
	
	e["obj"].css({
	    marginLeft:e.position[0] - e.itemWidth05 
	    ,marginTop:e.position[1] - e.itemHeight05 
	    ,msTransform:"rotate(" + e.angle + "deg)"
	    ,"-moz-transform":"rotate(" + e.angle + "deg)"
	    ,"-o-transform":"rotate(" + e.angle + "deg)"
	    ,"-webkit-transform":"rotate(" + e.angle + "deg)"
	   ,"transform":"rotate(" + e.angle + "deg)"
		//,"transform-origin": (e.itemWidth /2) +"px "+(e.itemHeight +10)+"px"
		//значение точки смещения подбирается опытным путем
		,"transform-origin": (e.itemWidth /2) +"px "+(200)+"px"
		});
	$("#traila").css({
	    marginLeft:e.position[0] - e.itemWidth05
	    ,marginTop:e.position[1] - e.itemHeight05 +250
	    ,msTransform:"rotate(" + e.angle + "deg)"
	    ,"-moz-transform":"rotate(" + tAngle[8] + "deg)"
	    ,"-o-transform":"rotate(" + tAngle[8] + "deg)"
	    ,"-webkit-transform":"rotate(" + tAngle[8] + "deg)"
	    ,"transform":"rotate(" + tAngle[8] + "deg)"
		//,"transform-origin": (e.itemWidth /2) +"px "+(0 )+"px"
		,"transform-origin": (83) +"px "+( -50 )+"px"
	});
		// заполняю масссив предэдущими значениями, что бы создать задержку смещения прицепа
		// по хорошему надо очищать перед каждым запуском.
		if(tAngle.length<=20){
			tAngle[tAngle.length + 1] = e.angle;
		}else{
			tAngle = tAngle.splice(0,1);
			tAngle[tAngle.length + 1] = e.angle;
		}
		//console.log("tAngle",tAngle, "e.angle", e.angle, tAngle.length);
			

  }, getID:function() {
    return++systemID
  },
	stop:function(step){
		clearInterval(works);
		//console.log("STOP");
		//console.log(options);
		if(options.items[0].direction){
				if(options.items[0].waypoint <= step){
					options.items[0].direction = true;
				}
				if(options.items[0].waypoint >= step){
					options.items[0].direction = false;
					temp = true;
				}
		}else{
				if(options.items[0].waypoint > step){
					options.items[0].direction = false;
				}			
				if(options.items[0].waypoint < step){
					options.items[0].direction = true;
					temp = true;
				}			
		}
		options.items[0].waypointEnd = step;
		//*
//		if(temp){
//		//	console.log('go');
//			temp2 = [options.items[0].waypoint, step, options.items[0].waypoints];
//			options.items[0].waypoints = getRadius([options.items[0].position[0],options.items[0].position[1]]);
//			options.items[0].waypointEnd = 180;
//		}
		//*/
		works = setInterval(function() {
					methods.process(processID)
				}, options.interval);
	},
	start:function(){
	
	}
  };
  $.fn.ways = function(method) {
    if(methods[method]) {
      return methods[method].apply(this, Array.prototype.slice.call(arguments, 1))
    }else {
      if(typeof method === "object" || !method) {
        return methods.init.apply(this, arguments)
      }else {
        $.error("Method " + method + " does not exist on jQuery.ways")
      }
    }
  }
})(jQuery);


function getRadius(start) { // некоторые аргументы определим на будущее
	var r = 450;
	var radius = [];
	var f = 0;
	var s = 2 * Math.PI / 180; //Вычислим угол
	for(var i = 0; i <=180; i++){
		f += s; // приращение аргумента
		  var left =  start[0] + r * Math.sin(f); // меняем координаты элемента, подобно тому как мы это делали в школе в декартовой системе координат. Правда, в данном случае используется полярная система координат, изменяя угол
		  var top =   start[1] + r * Math.cos(f);
		  radius[i] = [left,top];
	}
	return radius;
}