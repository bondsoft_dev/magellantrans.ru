<footer>
    <div class="container clearfix">
        <div class="copyright"><span>Права на копирование</span> ООО «МагелланТранс»</div>
        <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            "bottom.menu",
            array(
                "ALLOW_MULTI_SELECT" => "N",
                "CHILD_MENU_TYPE" => "left",
                "DELAY" => "N",
                "MAX_LEVEL" => "1",
                "MENU_CACHE_GET_VARS" => array(
                ),
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "ROOT_MENU_TYPE" => "bottom",
                "USE_EXT" => "Y",
                "COMPONENT_TEMPLATE" => "bottom_menu"
            ),
            false
        );?>
        <div class="audio">
            <div class="wrap">
                <img src="/h/images/icon-audio.png" alt=""/>
            </div>
        </div>
        <div class="dev">Сделанно в <a target="_blank" href="http://LaFedja.ru">LaFedja.ru</a></div>
    </div>
</footer>
<div id="preloader"></div>
<audio tabindex="0" id="beep-one" loop controls preload="auto" >
    <source src="/h/audio/thats-me.mp3">
</audio>
</body>
</html>
<!--
<script src="//api-maps.yandex.ru/2.0/?load=package.standard,package.geoObjects&lang=ru-RU" type="text/javascript"></script>
<script>
    ymaps.ready(init);

    function init () {
        var myMap = new ymaps.Map("map", {
            center: [45.04757035, 39.0175773],
            zoom: 16
        })
    }


</script>
-->