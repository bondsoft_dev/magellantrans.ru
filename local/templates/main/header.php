<!DOCTYPE html>
<html lang="ru">
<head>
    <title><?$APPLICATION->showTitle()?></title>    <meta name="format-detection" content="telephone=no"/>
    <meta charset = "utf-8" />
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <link rel="stylesheet" href="/h/css/slick.css" type="text/css">
    <link rel="stylesheet" href="/h/css/slick-theme.css" type="text/css">
    <link rel="stylesheet" href="/h/css/SelectBoxIt.css" type="text/css">
    <link rel="stylesheet" href="/h/css/jquery.fancybox.min.css" type="text/css">
    <link rel="stylesheet" href="/h/css/style.css" type="text/css">
    <script src="/h/js/jquery-1.11.2.min.js"></script>
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="/h/js/slick.min.js"></script>
    <script src="/h/js/SelectBoxIt.js"></script>
    <script src="/h/js/jquery.maskedinput.js"></script>
    <script src="/h/js/jquery.fancybox.min.js"></script>
  	<script src="/h/js/smooth-scrollbar.js"></script>


   

    <script src="/h/js/main.js"></script>
    <?$APPLICATION->showHead()?>
</head>
<body class="body-overflow">
<?$APPLICATION->showPanel()?>
<header>
    <div class="container">
        <?
           $APPLICATION->IncludeComponent(
            "bitrix:menu",
            "top.menu",
            array(
                "ALLOW_MULTI_SELECT" => "N",
                "CHILD_MENU_TYPE" => "left",
                "DELAY" => "N",
                "MAX_LEVEL" => "1",
                "MENU_CACHE_GET_VARS" => array(
                ),
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "ROOT_MENU_TYPE" => "top",
                "USE_EXT" => "Y",
                "COMPONENT_TEMPLATE" => "top_menu"
            ),
            false
        ); ?>
        <div class="panel">
            <div class="logo">
                <a href="/">
                    <img src="/h/images/logo.png" alt=""/>
                </a>
            </div>
            <div class="phone">
                <a href="tel:<?echo \COption::GetOptionString( "askaron.settings", "UF_PHONE" );?>">
                    <img src="/h/images/icon-phone.png" alt=""/>
                </a>
            </div>
        </div>
    </div>
    <div class="mob-nav">
        <span></span>
    </div>
</header>