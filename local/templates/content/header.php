<!DOCTYPE html>
<html lang="ru">
<head>
    <title><?$APPLICATION->showTitle()?></title>
    <meta name="format-detection" content="telephone=no"/>
    <meta charset = "utf-8" />
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/h/css/slick.css" type="text/css">
    <link rel="stylesheet" href="/h/css/slick-theme.css" type="text/css">
    <link rel="stylesheet" href="/h/css/SelectBoxIt.css" type="text/css">
    <link rel="stylesheet" href="/h/css/jquery.fancybox.min.css" type="text/css">
    <link rel="stylesheet" href="/h/css/style.css" type="text/css">
    <link rel="stylesheet" href="/h/css/stm_fonts/stm.css" type="text/css">
    <link rel="stylesheet" href="/h/css/iziModal.min.css">



    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<?if($APPLICATION->GetCurDir() == "/"):?>
    <script src="/h/js/jquery-1.11.2.min.js"></script>
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="/h/js/SelectBoxIt.js"></script>
    <script src="/h/js/main.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<?else:?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<?endif;?>


    <script src="/h/js/slick.min.js"></script>

    <script src="/h/js/jquery.maskedinput.js"></script>
    <script src="/h/js/jquery.fancybox.min.js"></script>

    <script src="/h/js/iziModal.min.js" type="text/javascript"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.validate.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/custom.js"></script>

    <?$APPLICATION->showHead()?>
</head>
<body <?if($APPLICATION->GetCurDir() == "/"):?>class="body-overflow"<?endif;?>>
<?$APPLICATION->showPanel()?>
<div class="page">
    <header>
        <div class="container top-header">
            <div class="top-header-items">
                <div class="top-header-item">
                    <input type="button" value="">
                </div>
                <div class="top-header-item phn">
                    <p>Телефон: 8 (800) 707-55-43</p>
                </div>
                <div class="top-header-item mail">
                    <p>info@magellantrans.ru</p>
                </div>
                <div class="top-header-item time">
                    <p>Пн — Сб: 9:00 — 18:00</p>
                </div>
                <div class="top-header-item small">
                <select class="custom-select " id="inputGroupSelect01">
                    <option selected>Choose...</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>
                </div>
            </div>
        </div>
        <div class="container clearfix">
            <?$APPLICATION->IncludeComponent(
                "bitrix:menu",
                "content.menu",
                array(
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "left",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "1",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "ROOT_MENU_TYPE" => "bottom",
                    "USE_EXT" => "Y",
                    "COMPONENT_TEMPLATE" => "bottom_menu"
                ),
                false
            );?>
            <div class="panel">
                <div class="logo">
                    <a href="/">
                        <img src="/h/images/logo.png" alt=""/>
                    </a>
                </div>
                <div class="phone">
                    <a href="tel:<?echo \COption::GetOptionString( "askaron.settings", "UF_PHONE" );?>">
                        <img src="/h/images/icon-phone.png" alt=""/>
                    </a>
                </div>
                <!-- <a href="tel:<?echo \COption::GetOptionString( "askaron.settings", "UF_PHONE" );?>" class="tel"><?echo \COption::GetOptionString( "askaron.settings", "UF_PHONE" );?></a> -->
            </div>
        </div>
        <div class="mob-nav">
            <span></span>
        </div>
    </header>

    <div>
        <div id="container-news" class="container">
            <div id="row-news" class="row">
                <div class="col-md-12">
<div id="content-header">
    <h1><?$APPLICATION->showTitle(false)?></h1>
</div>