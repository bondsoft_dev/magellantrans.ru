
                </div>
            </div>
        </div>
</div>
<!-- <footer>
    <div class="container clearfix">
        <div class="copyright"><span>Права на копирование</span> ООО «МагелланТранс»<br/><a href="#">Политика конфиденциальности</a></div>
        <div class="audio">
            <div class="wrap">
                <img src="/h/images/icon-audio.png" alt=""/>
            </div>
        </div>
        <div class="dev">Сделанно в <a target="_blank" href="http://LaFedja.ru">LaFedja.ru</a></div>
    </div>
</footer> -->


<?if($APPLICATION->GetCurDir()=="/uslugi-po-perevozke/"):?>
<div class="pre-footer container">
<div class="bottom-section">
	<div class="bottom-section-row">
		<a class="bottom-section-icon" href="/news/osobennosti-perevozki-negabaritnyih-gruzov/">
			<div class="bottom-section-icon-img">
			<i class="stm-transport875"></i>
			</div>
			<div class="bottom-section-icon-text">
				<span>ПЕРЕВОЗКА НЕГАБАРИТНЫХ ГРУЗОВ</span>
				<p>Уровень технического оснащения позволяет нам качественно осуществлять перевозку негабаритных грузов. Перевезем комбайны, трактора, строительную и дорожную технику, негабаритные металлоконструкции, трансформаторы, ёмкости и другие массивные грузы.</p>
			</div>
</a>
		<a class="bottom-section-icon" href="/news/kak-otpravit-gruz-po-zheleznoy-doroge-nyuansyi-perevozki-zh-d-transportom/">
			<div class="bottom-section-icon-img">
			<i class="stm-transport857"></i>
			</div>
			<div class="bottom-section-icon-text">
				<span>ЖЕЛЕЗНОДОРОЖНЫЕ ПЕРЕВОЗКИ</span>
				<p>Осуществляем полный цикл ЖД-перевозки, от предоставления подвижного состава до выгрузки. Оформление железнодорожных документов, повагонные отправки, крепление и раскрепление грузов, опломбирование вагона, перевозки грузов в контейнерах различной вместимости.</p>
			</div>
</a>
	</div>




	<div class="bottom-section-row">
		<a class="bottom-section-icon" href="/news/osobennosti-perevozok-krupnogabaritnyih-gruzov/">
			<div class="bottom-section-icon-img">
			<i class="stm-transport842"></i>
			</div>
			<div class="bottom-section-icon-text">
				<span>ПЕРЕВОЗКА ГАБАРИТНЫХ ГРУЗОВ</span>
				<p>Перевозка и экспедирование любого груза по России, а так же в страны СНГ, Евросоюза и средней Азии.</p>
			</div>
</a>
		<a class="bottom-section-icon" href="/news/perevozki-tyazhelovesnyih-gruzov-chto-nuzhno-uchest/">
			<div class="bottom-section-icon-img">
			<i class="stm-transport848"></i>
			</div>
			<div class="bottom-section-icon-text">
				<span>ПЕРЕВОЗКА ТЯЖЕЛОВЕСНЫХ ГРУЗОВ</span>
				<p>Перевозка собственным автотранспортом любых тяжеловесных грузов на любое расстояние!</p>
			</div>
</a>
		<a class="bottom-section-icon" href="/news/osobennosti-organizatsii-morskih-perevozok/">
			<div class="bottom-section-icon-img">
			<i class="stm-transport857"></i>
			</div>
			<div class="bottom-section-icon-text">
				<span>МОРСКИЕ ПЕРВОЗКИ / ФРАХТ</span>
				<p>Осуществляем полный цикл морских перевозок, от фрахта судна до оформления таможенных документов.</p>
			</div>
</a>
	</div>
</div>
<div class="questions">
	<div>
		<h2>Есть вопросы? Наши специалисты всегда на связи! </h2>
	</div>
	<div id="zakaz_call">
		<a href="#">Связаться </a>
	</div>
</div>
<div class="advantages">
	<div class="advantages-col-1">
		<img src="/h/images/advantages-3.png" alt="11111">
		<p>БЕЗОПАСНОСТЬ И ОТВЕТСТВЕННОСТЬ</p>
		<p>Стандарты нашей работы подразумевают полную безопасность при перевозке ваших грузов</p>
	</div>
	<div class="advantages-col-2">
		<img src="/h/images/advantages-2.png" alt="22222">
		<p>ТОЧНЫЕ СРОКИ</p>
		<p>Вы можете не беспокоится о сроках доставки.</p>
	</div>
	<div class="advantages-col-3">
		<img src="/h/images/advantages-1.png" alt="33333">
		<p>ВСЕГДА НА СВЯЗИ!</p>
		<p>Наши специалисты всегда готовы ответить на любой ваш вопрос.</p>
	</div>
</div>
</div>

<?endif;?>

<footer id="footer">
    <!-- <div class="widgets_row">
        <div class="container">
            <div class="footer_widgets">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <section id="text-2" class="widget widget_text">
                            <div class="textwidget">
                                <div class="item" data-lat="45.047549" data-lng="39.017617" data-title="Краснодар">
                                    <span itemscope="" itemtype="http://schema.org/Organization">
                                        <p></p>
                                        <div class="title_f"> <span itemprop="name">Магеллан в Краснодаре</span></div>
                                        <ul>
                                            <li>
                                                <div class="icon_f"> <i class="stm-location-2"></i> </div>
                                                <div class="text_f"> <span itemprop="address" itemscope=""
                                                        itemtype="http://schema.org/PostalAddress">
                                                        <p></p>
                                                        <p><span itemprop="postalCode">350029</span>, г.<span
                                                                itemprop="addressLocality"> Краснодар</span>,<br>
                                                            <span itemprop="streetAddress">ул.Первомайская, 69, оф.
                                                                207</span></p>
                                                        <p><span itemprop="telephone" style="display: none;">-</span>
                                                        </p>
                                                    </span></div>
                                            </li>
                                            <li>
                                                <div class="icon_f"><i class="stm-iphone"></i></div>
                                                <div class="text">
                                                </div>
                                            </li>
                                            <li>
                                                <div class="icon_f"><i class="stm-email"></i></div>
                                                <div class="text"><a href="mailto: info@magellantrans.ru"><span
                                                            itemprop="email"> info@magellantrans.ru</span></a></div>
                                            </li>
                                        </ul>
                                        <p></p>
                                    </span></div>
                            </div>
                        </section>

               

                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <section id="text-3" class="widget widget_text">
                            <div class="textwidget">
                                <div class="item" data-lat="55.707475" data-lng="37.687859" data-title="Москва">
                                    <span itemscope="" itemtype="http://schema.org/Organization">
                                        <p></p>
                                        <div class="title_f"><span itemprop="name">Магеллан в Москве</span></div>
                                        <ul>
                                            <li>
                                                <div class="icon_f"><i class="stm-location-2"></i></div>
                                                <div class="text_f"><span itemprop="address" itemscope=""
                                                        itemtype="http://schema.org/PostalAddress">
                                                        <p></p>
                                                        <p><span itemprop="postalCode">115193</span>, г.<span
                                                                itemprop="addressLocality"> Москва</span>,<br>
                                                            <span itemprop="streetAddress"> ул. Южнопортовая, д.5,
                                                                оф.305</span></p>
                                                        <p><br>
                                                            <span itemprop="telephone" style="display: none;">-</span>
                                                        </p>
                                                    </span></div>
                                            </li>
                                            <li>
                                                <div class="icon_f"> <i class="stm-iphone"></i> </div>
                                                <div class="text">
                                                    <p></p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="icon_f"> <i class="stm-email"></i></div>
                                                <div class="text"><a href="mailto: info@magellantrans.ru"> <span
                                                            itemprop="email">info@magellantrans.ru</span></a></div>
                                            </li>
                                        </ul>
                                        <p></p>
                                    </span></div>
                            </div>
                        </section>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <section id="advanced_text-2" class="widget widget_advanced_text">
                            <div class="textwidget ">
                                <div class="item" data-lat="59.905855" data-lng="30.264239"
                                    data-title="Санкт-Петербург">
                                    <span itemscope="" itemtype="http://schema.org/Organization">
                                        <p></p>
                                        <div class="title_f"><span itemprop="name">Магеллан в Санкт-Петербурге</span>
                                        </div>
                                        <ul>
                                            <li>
                                                <div class="icon_f"><i class="stm-location-2"></i></div>
                                                <div class="text_f"><span itemprop="address" itemscope=""
                                                        itemtype="http://schema.org/PostalAddress">
                                                        <p></p>
                                                        <p><span itemprop="postalCode">190020</span>, г.<span
                                                                itemprop="addressLocality"> Санкт-Петербург</span>,<br>
                                                            <span itemprop="streetAddress"> ул. Бумажная д. 16, корпус
                                                                1, литер "А",<br>
                                                                оф. 218. БЦ "Портал"</span></p>
                                                        <p><span itemprop="telephone" style="display: none;">-</span>
                                                        </p>
                                                    </span></div>
                                            </li>
                                            <li>
                                                <div class="icon_f"> <i class="stm-iphone"></i> </div>
                                                <div class="text">
                                                    <p></p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="icon_f"> <i class="stm-email"></i></div>
                                                <div class="text"><a href="mailto: info@magellantrans.ru"> <span
                                                            itemprop="email">info@magellantrans.ru</span></a></div>
                                            </li>
                                        </ul>
                                        <p></p>
                                    </span></div>
                            </div>
                        </section>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <section id="text-4" class="widget widget_text">
                            <div class="textwidget"><br>
                                <div class="item" data-lat="59.905855" data-lng="30.264239" data-title="Ростов-на-Дону">
                                    <span itemscope="" itemtype="http://schema.org/Organization">
                                        <div class="title_f"><span itemprop="name">Магеллан в Ростове-на-Дону</span>
                                        </div>
                                        <ul>
                                            <li>
                                                <div class="icon_f"><i class="stm-location-2"></i></div>
                                                <div class="text_f"><span itemprop="address" itemscope=""
                                                        itemtype="http://schema.org/PostalAddress">
                                                        <p><span itemprop="postalCode">190020</span>, г.<span
                                                                itemprop="addressLocality">Ростов-на-Дону</span>,
                                                            <span itemprop="streetAddress"> ул. Социалистическая, д. 74,
                                                                оф. 1502</span></p>
                                                    </span><span itemprop="telephone" style="display: none;">-</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="icon_f"> <i class="stm-iphone"></i> </div>
                                                <div class="text">
                                                    <p></p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="icon_f"> <i class="stm-email"></i></div>
                                                <div class="text"><a href="mailto: info@magellantrans.ru"> <span
                                                            itemprop="email">info@magellantrans.ru</span></a></div>
                                            </li>
                                        </ul>
                                    </span></div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div> -->




    <div class="footer-dop container">
        <div class="footer-row">
        <h3 style="
    color: #fff;
    padding: 20px;
    font-size: 28px;
">
                Наши филиалы
            </h3>
        </div>
        <div class="footer-items">
            <div class="footer-item">
                <span>КРАСНОДАР</span>
                <div class="footer-item-geo">
                    <p>350029, г. Краснодар,ул.Первомайская, 69, оф. 207</p>
                </div>
                <div class="footer-item-mail">   
                    <a href="mailto:info@magellantrans.ru">info@magellantrans.ru</a>
                </div>    
            </div>
            <div class="footer-item">
                <span>МОСКВА</span>
                <div class="footer-item-geo">
                    <p>115193, г. Москва,ул. Южнопортовая, д.5, оф.305</p>
                </div> 
                <div class="footer-item-mail">   
                    <a href="mailto:info@magellantrans.ru">info@magellantrans.ru</a>
                </div>
            </div>
            <div class="footer-item">
                <span>САНКТ-ПЕТЕРБУРГ</span>
                <div class="footer-item-geo">
                    <p>190020, г. Санкт-Петербург,ул. Бумажная д. 16, корпус 1, литер "А", оф. 218. БЦ "Портал"</p>
                </div>
                <div class="footer-item-mail">
                    <a href="mailto:info@magellantrans.ru">info@magellantrans.ru</a>
                </div>    
            </div>
            <div class="footer-item">
                <span>РОСТОВЕ-НА-ДОНУ</span>    
                <div class="footer-item-geo">
                    <p >190020, г.Ростов-на-Дону, ул. Социалистическая, д. 74, оф. 1502</p>
                </div>
                <div class="footer-item-mail">
                  <a href="mailto:info@magellantrans.ru">info@magellantrans.ru</a>  
                </div>    
                
            </div>
        </div>
    </div>
</footer>

    

 <div id="callback" class="black">
        <form id="form-callback" class="ajax-form-callback" action="/local/templates/content/ajax/send.php" method="POST">
            <input id="name" name="name" type="text" placeholder="Имя:">
            <input id="phn" name="phn" type="text" placeholder="Телефон:">
            <input id="send" type="submit" class="btn button waves-effect waves-light text-uppercase" value="Отправить"> 
            <p>Нажимая на кнопку, вы соглашаетесь с <a href="/Politika-Magellan.pdf" class="underline">политикой конфиденциальности</a></p></form>
            <br>
</div>


<div id="modal-error"></div>
<div id="modal-success"></div>



<div id="auto"></div>
</div>

</body>
</html>
<!--
<script src="//api-maps.yandex.ru/2.0/?load=package.standard,package.geoObjects&lang=ru-RU" type="text/javascript"></script>
<script>
    ymaps.ready(init);

    function init () {
        var myMap = new ymaps.Map("map", {
            center: [45.04757035, 39.0175773],
            zoom: 16
        })
    }


</script>
-->