

$(document).ready(function(){
    
    $(window).scrollTop(0, function() {
        $(".top-header").remove();
    });




    var valid = {
        errorElement: "div",
        rules: {
           type: {
               required: false
           }
           , from: {
               required: true
               , minlength: 2
           }
           , in: {
               required: true
               , minlength: 2
           }
           , phn: {
               required: true
               , minlength: 11
           }
           , weight: {
               required: false
           }
           , size: {
               required: false
           }
           , email: {
               required: false
               ,email:true
           }
           , dop: {
               required: false
           }
           , name: {
            required: true
        }         
       , }
       , messages: {
           from: {
            required: "(это обязательное поле)"
            , minlength: "(введите город отправления)"
           }
           , in: {
               required: "(это обязательное поле)"
               , minlength: "(введите город прибытия)"
           }
           , phn: {
               required: "(это обязательное поле)"
               , minlength: "(введите номер полностью)"
           }
           , name: {
            required: "(это обязательное поле)"
            , minlength: "(введите номер полностью)"
        }
       , }
       , submitHandler: function (form) {
           //Отправка формы
           var formData = $(form).serialize()
               , action = $(form).attr('action');
           $.ajax({
               method: 'POST'
               , data: formData
               , dataType: 'json'
               , url: action
               , success: function (data) {
   
                   if (data.success == 1) {
                    $('#callback').iziModal('close');
                    $('#modal-success').iziModal('open');
                   }
                   else {
                    $('#callback').iziModal('close');
                       $('#modal-error').iziModal('open');
                   }
               }
               
           });
           return false;
       }
   }
   
  


   $("#callback").iziModal({
    title: 'Заказать обратный звонок'
    , subtitle: 'Перезвоним как можно скорее'
    , headerColor: '#2b2b2b'
    , icon: null
    , background: null
    , iconText: null
    , /*iconColor: '',*/
    rtl: false
    , width: 600
    , top: null
    , bottom: null
    , borderBottom: true
    , padding: 18
    , radius: 0
    , /*fullscreen: true,*/
    closeOnEscape: true
    , closeButton: true
    , zindex: 999
, });
$("#modal-success").iziModal({
        title: "Ваше сообщение отправлено"
        , subtitle: "Мы обязательно с Вами свяжемся"
        , icon: null
        , headerColor: '#34ccff'
        , width: 600
        , timeout: 4000
        , timeoutProgressbar: true
        , transitionIn: 'fadeInUp'
        , transitionOut: 'fadeOutDown'
        , bottom: 0
        , loop: true
        , pauseOnHover: true
    });
    $("#modal-error").iziModal({
        title: "Произошла ошибка"
        , subtitle: "Попробуйте отправить ваще обращение снова"
        , icon: null
        , headerColor: '#BE0F18'
        , width: 600
        , timeout: 4000
        , timeoutProgressbar: true
        , transitionIn: 'fadeInUp'
        , transitionOut: 'fadeOutDown'
        , bottom: 0
        , loop: true
        , pauseOnHover: true
    });
    $(document).on('click', '#zakaz_call', function (event) {
        event.preventDefault();
        $('#callback').iziModal('open');
    }); 
    // $(document).on('click', '#send', function (event) {
    //     event.preventDefault();
    //     $('#callback').iziModal('close');
    //     $('#modal-success').iziModal('open');
    // }); 

    $('.form-rasch').validate(valid);
    $('#form-callback').validate(valid);
    $(".news-item a").fancybox();

})




