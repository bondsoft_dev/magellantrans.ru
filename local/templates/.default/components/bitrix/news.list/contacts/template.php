<div class="section-contact" id="section-contact">
    <div class="head">Контакты<br/>
        компании</div>
    <div class="content" data-scrollbar>
        <div class="address-wrap">
            <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/include/address.php"
                )
            );?>
        </div>
        <?foreach ($arResult["SECTIONS"] as $arSection):?>
        <div class="title">Отдел продаж</div>
        <?if($arSection["ITEMS"]):?>
        <div class="team-wrap">
            <?foreach ($arSection["ITEMS"] as $arItem):?>
            <div class="item">
                <div class="image">
                    <?if($arItem["PREVIEW_PICTURE"]):?>
                    <img src="<?=CFile::GetPath($arItem["PREVIEW_PICTURE"])?>" alt="<?=$arItem["NAME"]?>"/>
                    <?else:?>
                    <img src="/h/images/photo_1.png" alt="<?=$arItem["NAME"]?>"/>
                    <?endif?>
                </div>
                <div class="text">
                    <div class="name"><?=$arItem["NAME"]?></div>
                    <?if($arItem["PROPERTIES"]["POSITION"]["VALUE"]):?>
                    <div class="position"><?=$arItem["PROPERTIES"]["POSITION"]["VALUE"]?></div>
                    <?endif;?>
                    <ul>
                        <?if($arItem["PROPERTIES"]["PHONE"]["VALUE"]):?>
                            <li><?=$arItem["PROPERTIES"]["PHONE"]["VALUE"]?></li>
                        <?endif;?>
                        <?if($arItem["PROPERTIES"]["EMAIL"]["VALUE"]):?>
                            <li><?=$arItem["PROPERTIES"]["EMAIL"]["VALUE"]?></li>
                        <?endif;?>
                        <?if($arItem["PROPERTIES"]["ADDRESS"]["VALUE"]):?>
                            <li><?=$arItem["PROPERTIES"]["ADDRESS"]["VALUE"]?></li>
                        <?endif;?>
                    </ul>
                </div>
            </div>
            <?endforeach;?>
        </div>
        <?endif;?>
        <?endforeach;?>
    </div>
    <div class="map" id="map">

    </div>
</div>