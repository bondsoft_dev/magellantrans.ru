<? //var_dump($arResult); ?>
<div class="section-media" id="section-media">
    <div class="head">
        медиа<br/>
        галерея
    </div>
    <div class="content">
        <?foreach ($arResult["SECTIONS"] as $key => $section):?>
        <div class="slider slider-for" data-slider="<?=$key?>">
            <?foreach ($section["ITEMS"] as $arItem):?>
            <div>
                <div class="image" style="background-image: url('<?=$arItem["DETAIL_PICTURE"]["SRC"]?>')"></div>
            </div>
            <?endforeach;?>
        </div>
        <?endforeach;?>
        <div class="panel">
            <div class="title">подгалереи</div>
            <div class="slider slider-nav">
                <?foreach ($arResult["SECTIONS"] as $key => $section):?>
                <div>
                    <div class="item" data-slider="<?=$key?>">
                        <div class="image">
                            <img src="<?=CFile::GetPath($section["PICTURE"])?>" alt="<?=$section["NAME"]?>"/>
                        </div>
                        <div class="tit"><?=$section["NAME"]?></div>
                    </div>
                </div>
                <?endforeach;?>
            </div>
        </div>
    </div>
</div>