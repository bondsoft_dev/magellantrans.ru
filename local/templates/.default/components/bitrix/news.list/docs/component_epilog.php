<?php
if($_GET["ajax"] == "Y"){
    ob_get_clean();
    ?>
    <div class="section-doc" id="section-doc">
        <div class="head">
            документы<br/>
            для скачивания
        </div>
        <div class="content" data-scrollbar>
            <? //var_dump($arResult) ?>
            <?foreach ($arResult["SECTIONS"] as $arSection):?>
            <div class="tit"><?=$arSection["NAME"]?></div>
            <?if($arSection["ITEMS"]):?>
                <div class="doc-wrap">
                    <?foreach ($arSection["ITEMS"] as $arItem):?>
                    <?$file = CFile::GetPath($arItem["PROPERTIES"]["FILE"]["VALUE"]);?>
                        <div class="item">
                        <div class="icon">
                            <?if(GetFileExtension($file) == "pdf"):?>
                            <img src="/h/images/icon-pdf.png" alt=""/>
                            <?else:?>
                            <img src="/h/images/icon-word.png" alt=""/>
                            <?endif;?>
                    </div>
                        <div class="text">
                            <p><?=$arItem["NAME"]?></p>
                        </div>
                        <a href="<?=$file?>" target="_blank" class="download">Скачать</a>
                        </div>
                    <?endforeach;?>
                </div>
            <?endif;?>
            <?endforeach;?>
        </div>
        <a href="#" class="btn">+7 (861) 25-777-95</a>
        <div class="info">По вопросам документооборота звоните</div>
    </div>
<?
    die();
}