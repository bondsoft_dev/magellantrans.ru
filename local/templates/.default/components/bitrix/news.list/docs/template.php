<div class="section-doc" id="section-doc">
    <div class="head">
        документы<br/>
        для скачивания
    </div>
    <div class="content" data-scrollbar>
        <?foreach ($arResult["SECTIONS"] as $arSection):?>
            <div class="tit"><?=$arSection["NAME"]?></div>
            <?if($arSection["ITEMS"]):?>
                <div class="doc-wrap">
                    <?foreach ($arSection["ITEMS"] as $arItem):?>
                        <?$file = CFile::GetPath($arItem["PROPERTIES"]["FILE"]["VALUE"]);?>
                        <div class="item">
                            <div class="icon">
                                <?if(GetFileExtension($file) == "pdf"):?>
                                    <img src="/h/images/icon-pdf.png" alt=""/>
                                <?else:?>
                                    <img src="/h/images/icon-word.png" alt=""/>
                                <?endif;?>
                            </div>
                            <div class="text">
                                <p><?=$arItem["NAME"]?></p>
                            </div>
                            <a href="<?=$file?>" target="_blank" class="download">Скачать</a>
                        </div>
                    <?endforeach;?>
                </div>
            <?endif;?>
        <?endforeach;?>
    </div>
    <a href="tel:<?echo \COption::GetOptionString( "askaron.settings", "UF_PHONE" );?>" class="btn"><?echo \COption::GetOptionString( "askaron.settings", "UF_PHONE" );?></a>
    <div class="info">По вопросам документооборота звоните</div>
</div>
