<?php
$arFilter = Array('IBLOCK_ID'=>3, 'ACTIVE'=>'Y', 'GLOBAL_ACTIVE'=>'Y');

$db_list = \CIBlockSection::GetList($sort, $arFilter, true, $select);

$sections = [];

while($ar_result = $db_list->GetNext())
{
    $section = $ar_result;
    $arSelect = Array("ID", "IBLOCK_ID", "NAME");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
    $arFilter = Array("IBLOCK_ID"=>$arResult["ORIGINAL_PARAMETERS"]["IBLOCK_ID"], "SECTION_CODE"=>$ar_result["CODE"], "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    $sectionData = [];
    while($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        $arProps = $ob->GetProperties();
        $data = $arFields;
        $data["PROPERTIES"] = $arProps;
        $sectionData[] = $data;
    }
    $section["ITEMS"] = $sectionData;
    $sections[] = $section;
}

$arResult["SECTIONS"] = $sections;
$this->__component->arResultCacheKeys = array_merge($this->__component->arResultCacheKeys, array('SECTIONS'));


