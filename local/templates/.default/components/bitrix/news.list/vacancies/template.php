<div class="section-vacancy" id="section-vacancy">
    <div class="head">
        Наши<br/>
        вакансии
    </div>
    <div class="content" data-scrollbar>
        <div class="vacancy-wrap">
            <?foreach ($arResult["ITEMS"] as $arItem):?>
            <div class="item">
                <div class="date"><?=FormatDate("j F Y", MakeTimeStamp($arItem["ACTIVE_FROM"]))?></div>
                <div class="text">
                    <div class="tit"><?=$arItem["NAME"]?></div>
                    <?=$arItem["PREVIEW_TEXT"]?>
                </div>
            </div>
            <?endforeach;?>
        </div>
    </div>
</div>