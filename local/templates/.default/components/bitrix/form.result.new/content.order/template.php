<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="form-calc">
    <?=$arResult["FORM_HEADER"]?>
        <div class="calc">
            <div class="title">расчитать стоимость перевозки</div>
            <div class="line">
                <div class="col">
                    <label>Как к Вам обращаться</label>
                    <input name="<?='form_'.$arResult["QUESTIONS"]["NAME"]["STRUCTURE"][0]["FIELD_TYPE"].'_'.$arResult["QUESTIONS"]["NAME"]["STRUCTURE"][0]["ID"]?>" required type="text"/>
                </div>
                <div class="col">
                    <label>Телефон для обратной связи</label>
                    <input name="<?='form_'.$arResult["QUESTIONS"]["PHONE"]["STRUCTURE"][0]["FIELD_TYPE"].'_'.$arResult["QUESTIONS"]["PHONE"]["STRUCTURE"][0]["ID"]?>" required type="text"/>
                </div>
            </div>
        </div>
    <input type="hidden" name="web_form_submit" value="Сохранить">

    <div class="bottom">
            <div class="check">
                <input type="checkbox" checked class="checkbox-btn" id="checkbox-1" required placeholder="">
                <label for="checkbox-1">Согласен на обработку персональных данных.</label>
            </div>
            <button id="submit-form" type="submit">Отправить заявку</button>
        </div>

    <script>
        $('#submit-form').click(function (event) {
            let form = $('form[name=ORDERS2]');
            form.attr("action", "<?$APPLICATION->GetCurDir();?>");
            $.ajax({
                url:"<?$APPLICATION->GetCurDir();?>",
                method: "POST",
                data: form.serialize() + "&action=sendForm",
            })
                .done(function (data) {
                    alert("Спасибо за отзыв!");
                    document.getElementsByName("ORDERS2")[0].reset();
                });
            event.preventDefault();
        });
    </script>
    <?=$arResult["FORM_FOOTER"]?>
</div>
