<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$request = \Bitrix\Main\Context::getCurrent()->getRequest();
$formId = $request->get("WEB_FORM_ID");
$status = $request->get("formresult");

if(isset($formId) && $formId == 2)
{
    ob_get_clean();

    if($formId == $arResult["arForm"]["ID"] && $status == "addok")
    {
        echo json_encode(["status" => true, "mes" => "Спасибо за отзыв!"]);
        exit();
    }
}