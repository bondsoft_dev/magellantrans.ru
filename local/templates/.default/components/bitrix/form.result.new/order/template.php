<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?=$arResult["FORM_HEADER"]?>
<div class="line">
    <div class="col">
        <label>Компания</label>
        <input name="<?='form_'.$arResult["QUESTIONS"]["COMPANY"]["STRUCTURE"][0]["FIELD_TYPE"].'_'.$arResult["QUESTIONS"]["COMPANY"]["STRUCTURE"][0]["ID"]?>" required type="text"/>
    </div>
    <div class="col">
        <label>Пункт отправки</label>
        <input name="<?='form_'.$arResult["QUESTIONS"]["SENDING_POINT"]["STRUCTURE"][0]["FIELD_TYPE"].'_'.$arResult["QUESTIONS"]["SENDING_POINT"]["STRUCTURE"][0]["ID"]?>" required type="text"/>
    </div>
</div>
<div class="line">
    <div class="col">
        <label>Контактное лицо</label>
        <input name="<?='form_'.$arResult["QUESTIONS"]["PERSON"]["STRUCTURE"][0]["FIELD_TYPE"].'_'.$arResult["QUESTIONS"]["PERSON"]["STRUCTURE"][0]["ID"]?>" required type="text"/>
    </div>
    <div class="col">
        <label>Пункт прибытия</label>
        <input name="<?='form_'.$arResult["QUESTIONS"]["ARRIVAL_POINT"]["STRUCTURE"][0]["FIELD_TYPE"].'_'.$arResult["QUESTIONS"]["ARRIVAL_POINT"]["STRUCTURE"][0]["ID"]?>" required type="text"/>
    </div>
</div>
<div class="line">
    <div class="col">
        <label>E-mail для ответа</label>
        <input name="<?='form_'.$arResult["QUESTIONS"]["EMAIL"]["STRUCTURE"][0]["FIELD_TYPE"].'_'.$arResult["QUESTIONS"]["EMAIL"]["STRUCTURE"][0]["ID"]?>" required type="email"/>
    </div>
    <div class="col">
        <label>Примерный вес</label>
        <input name="<?='form_'.$arResult["QUESTIONS"]["WEIGHT"]["STRUCTURE"][0]["FIELD_TYPE"].'_'.$arResult["QUESTIONS"]["WEIGHT"]["STRUCTURE"][0]["ID"]?>" required type="text"/>
    </div>
</div>
<div class="line">
    <label>Подробности</label>
    <textarea name="<?='form_'.$arResult["QUESTIONS"]["MSG"]["STRUCTURE"][0]["FIELD_TYPE"].'_'.$arResult["QUESTIONS"]["MSG"]["STRUCTURE"][0]["ID"]?>" required></textarea>
</div>
<input type="hidden" name="web_form_submit" value="Сохранить">
<div class="line">
    <button id="submit-form">Отправить</button>
</div>
<script>
    $('#submit-form').click(function (event) {
        let form = $('form[name=ORDERS]');
        form.attr("action", "/order/");
        $.ajax({
            url:"/order/",
            method: "POST",
            data: form.serialize() + "&action=sendForm",
        })
            .done(function (data) {
                alert("Спасибо за отзыв!");
                document.getElementsByName("ORDERS")[0].reset();
            });
        event.preventDefault();
    });
</script>
<?=$arResult["FORM_FOOTER"]?>